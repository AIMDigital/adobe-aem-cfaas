# Adobe AEM - Content Fragments As A Service

This is a small Adobe AEM project aiming to demonstrate how it is possible to make use of Sling Model Exporters to expose
Content Fragments as a service.

## Modules

The main parts of the template are:

* core: Java bundle containing all core functionality bundled as an OSGi bundle.
* ui.content: contains a sample content fragment model and instance based on We.Retail.

## Dependencies
* Adobe Experience Manager (AEM) version 6.4 (likely to work with 6.3, but not tested).
* We.Retail sample AEM implementation.

## How to build

To build all the modules run in the project root directory the following command with Maven 3:

    mvn clean install

If you have a running AEM instance you can build and package the whole project and deploy into AEM with  

    mvn clean install -PautoInstallPackage
    
Or to deploy it to a publish instance, run

    mvn clean install -PautoInstallPackagePublish
    
Or alternatively

    mvn clean install -PautoInstallPackage -Daem.port=4503

Or to deploy only the bundle to the author, run

    mvn clean install -PautoInstallBundle

## Testing

No unit tests have been written for this project as it is purely a proof of concept.
