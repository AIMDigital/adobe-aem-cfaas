package net.aimdigital.aem.cfaas.core.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Model(
        adaptables = {Resource.class},
        resourceType = ListOfPagesModel.LIST_OF_PAGES_RESOURCE_TYPE,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL
)
public class ListOfPagesModel {

    protected static final String LIST_OF_PAGES_RESOURCE_TYPE = "conf/we-retail/settings/dam/cfm/models/list-of-pages";

    @Self
    private Resource resource;

    @ValueMapValue
    @JsonIgnore
    @Getter
    private String[] paths;

    @Getter
    private List<PageModel> pages;


    @PostConstruct
    public void init() {

        ResourceResolver resolver = resource.getResourceResolver();

        if (resolver != null) {
            this.pages = Arrays.stream(paths)
                    .map(path -> resolver.resolve(path))
                    .filter(Objects::nonNull)
                    .map(resource -> resource.adaptTo(PageModel.class))
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
        }
    }

}
