package net.aimdigital.aem.cfaas.core.models;

import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

import javax.inject.Inject;
import javax.inject.Named;

import static com.day.cq.commons.jcr.JcrConstants.JCR_DESCRIPTION;
import static com.day.cq.commons.jcr.JcrConstants.JCR_TITLE;
import static com.day.cq.wcm.api.NameConstants.PN_NAV_TITLE;
import static org.apache.jackrabbit.JcrConstants.JCR_CONTENT;

@Model(
        adaptables = {Resource.class},
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL
)
public class PageModel {

    @Getter
    @Inject
    @Named(JCR_CONTENT + "/" + JCR_TITLE)
    private String title;

    @Getter
    @Inject
    @Named(JCR_CONTENT + "/subtitle")
    private String subtitle;

    @Getter
    @Inject
    @Named(JCR_CONTENT + "/" + PN_NAV_TITLE)
    private String navTitle;

    @Getter
    @Inject
    @Named(JCR_CONTENT + "/" + JCR_DESCRIPTION)
    private String description;

    @Getter
    @Inject
    @Named(JCR_CONTENT + "/image/filePath")
    private String image;

}
