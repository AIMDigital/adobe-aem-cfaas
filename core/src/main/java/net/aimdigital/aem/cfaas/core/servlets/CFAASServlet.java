package net.aimdigital.aem.cfaas.core.servlets;

import net.aimdigital.aem.cfaas.core.resources.ContentFragmentResource;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.models.factory.ExportException;
import org.apache.sling.models.factory.MissingExporterException;
import org.apache.sling.models.factory.ModelFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import javax.servlet.Servlet;
import java.io.IOException;

import static org.apache.commons.collections.MapUtils.EMPTY_MAP;

import static org.apache.sling.api.servlets.ServletResolverConstants.SLING_SERVLET_EXTENSIONS;
import static org.apache.sling.api.servlets.ServletResolverConstants.SLING_SERVLET_METHODS;
import static org.apache.sling.api.servlets.ServletResolverConstants.SLING_SERVLET_RESOURCE_TYPES;
import static org.apache.sling.api.servlets.ServletResolverConstants.SLING_SERVLET_SELECTORS;

@Component(
        service = Servlet.class,
        property = {
            SLING_SERVLET_RESOURCE_TYPES + "=sling/servlet/default",
            SLING_SERVLET_METHODS + "=GET",
            SLING_SERVLET_EXTENSIONS + "=json",
            SLING_SERVLET_SELECTORS + "=cfaas" })
public class CFAASServlet extends SlingSafeMethodsServlet {

    private static final long serialVersionUID = 1L;


    private static final String CF_DATA_NODE_PATH = "/jcr:content/data";
    private static final String CF_MASTER_VARIANT = "/master";


    @Reference
    private transient ModelFactory modelFactory;

    private String modelJsonString;


    @Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws IOException {

        Resource currentResource = request.getResource();
        ResourceResolver resolver = request.getResourceResolver();

        //TODO implement better error handling for negative scenarios
        if (currentResource != null) {

            //TODO handle other variants - possibly via selectors
            Resource cfResource = resolver.resolve(currentResource.getPath() + CF_DATA_NODE_PATH + CF_MASTER_VARIANT);

            if (cfResource != null) {

                Resource wrappedCfResource = new ContentFragmentResource(cfResource);

                try {
                    this.modelJsonString = modelFactory.exportModelForResource(wrappedCfResource, "jackson", String.class, EMPTY_MAP);
                } catch (ExportException e) {
                    e.printStackTrace();
                } catch (MissingExporterException e) {
                    e.printStackTrace();
                }
            }
        }

        response.getWriter().print(this.modelJsonString);

    }

}
