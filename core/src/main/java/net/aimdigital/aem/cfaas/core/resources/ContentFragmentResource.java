package net.aimdigital.aem.cfaas.core.resources;

import com.day.cq.commons.inherit.ComponentInheritanceValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceWrapper;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.wrappers.CompositeValueMap;
import org.apache.sling.api.wrappers.ValueMapDecorator;

import java.util.HashMap;

import static org.apache.commons.lang3.StringUtils.*;

public class ContentFragmentResource extends ResourceWrapper {

    private static final String CF_MODEL_PATH_PROPERTY = "cq:model";

    private String resourceType;

    private ValueMap properties;


    public ContentFragmentResource(final Resource resource) {

        super(resource);

        this.resourceType = convertContentFragmentModelPathToResourceType(resource);

        final ValueMap overlayProperties = new ValueMapDecorator(new HashMap<>());
        overlayProperties.put(ResourceResolver.PROPERTY_RESOURCE_TYPE, this.resourceType);

        this.properties = new CompositeValueMap(overlayProperties, super.getValueMap());
    }


    private String convertContentFragmentModelPathToResourceType(Resource resource) {

        InheritanceValueMap inheritanceValueMap =  new ComponentInheritanceValueMap(resource);
        String cfModelPath = inheritanceValueMap.getInherited(CF_MODEL_PATH_PROPERTY, String.class);

        return isNotBlank(cfModelPath) ? removeStart(cfModelPath, "/") : EMPTY;
    }


    @Override
    public <AdapterType> AdapterType adaptTo(Class<AdapterType> type) {

        if (type != ValueMap.class) {
            return super.adaptTo(type);
        }

        return (AdapterType) this.getValueMap();
    }


    @Override
    public ValueMap getValueMap() {

        return this.properties;
    }


    @Override
    public String getResourceType() {

        return this.resourceType;
    }


    @Override
    public boolean isResourceType(String resourceType) {

        return this.getResourceResolver().isResourceType(this, resourceType);
    }
}
